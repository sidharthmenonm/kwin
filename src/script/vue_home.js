import Vue from 'vue';
import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);
var app = new Vue({
  el: '#app',
  data: {
    id: '1RkdG1jBTUVfXyPKFLjkR0Ld3Hrv9vNemFf9AolWlgjI',
    speakers: [],
    schedule: [],
    investors: [],
    active_sched: 0,
  },
  computed: {
    dates() {
      return this.getColUniq('date')
    },
    events() {
      return this.getColUniq('event')
    },
    sortedSpeakers: function() {
      return this.sortedItem(this.speakers, 'order')
    },
    sortedInvestors: function() {
      return this.sortedItem(this.investors, 'order')
    }
  },
  methods: {
    sortedItem: function(item, col) {
      function compare(a, b) {
        if (a[col] < b[col])
          return -1;
        if (a[col] > b[col])
          return 1;
        return 0;
      }

      return item.sort(compare);
    },
    loadData(sheet, callback) {
      fetch(`https://gsapi.futurelabkerala.in/api?id=${this.id}&sheet=${sheet}&columns=false&integers=false`)
        .then(response => response.json())
        .then(callback)
    },
    getColUniq: function(col) {
      var column = this.schedule.map(function(item) {
        return item[col];
      })
      return [...new Set(column)];
    },
    dated: function(date) {
      return this.schedule.filter(function(item) {
        return item.date == date;
      });
    },
    evented: function(event) {
      var evented = this.schedule.filter(function(item) {
        return item.event == event;
      });

      var dates = evented.map(function(item) {
        return item['date'];
      });

      var udate = [...new Set(dates)];

      return udate.map(date => {
        var i = {};
        i[date] = evented.filter(function(item) {
          return item.date == date;
        });
        return i;
      })

    },
    gdrive(url) {
      try {
        var id = url.match(/drive\.google\.com\/file\/d\/(.*?)\//)[1];
        return "https://drive.google.com/thumbnail?id=" + id;

      } catch (e) {
        return "https://drive.google.com/thumbnail?id=1-JTBMH4OjOtfrueHU99D0vGIJ4pq3DWu";
      }
    }
  },
  mounted() {
    this.loadData(1, (json) => {
      this.schedule = json.rows;
    })
    this.loadData(2, (json) => {
        this.speakers = json.rows;
      })
      // this.loadData(3, (json) => {
      //   this.investors = json.rows;
      // })
  }
});